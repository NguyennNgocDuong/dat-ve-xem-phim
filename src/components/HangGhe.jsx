import React, { Component } from "react";
import { connect } from "react-redux";
import { DAT_VE } from "../redux/constants/constantDatVe";

class HangGhe extends Component {
  renderNumberSeats = () => {
    // console.log(this.props.rowSeat);
    return this.props.rowSeat.danhSachGhe.map((numberSeat, index) => {
      return (
        <button
          onClick={() => this.props.datVe(numberSeat)}
          key={index}
          className="col ghe"
        >
          {numberSeat.soGhe}
        </button>
      );
    });
  };

  render() {
    return (
      <div className="row">
        <div className="rowNumber">{this.props.hang}</div>

        {this.renderNumberSeats()}
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    datVe: (numberSeat) => {
      dispatch({
        type: DAT_VE,
        payload: numberSeat,
      });
    },
  };
};
export default connect(null, mapDispatchToProps)(HangGhe);
