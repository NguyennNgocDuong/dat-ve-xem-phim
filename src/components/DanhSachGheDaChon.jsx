import React, { Component } from "react";
import { connect } from "react-redux";
import ".././css/BaiTapBookingTicket.css";

class DanhSachGheDaChon extends Component {
  renderDanhSachVeDaChon = () => {
    return this.props.danhSachGheDaChon.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.soGhe}</td>
          <td>{item.gia}</td>
          <td>Hủy</td>
        </tr>
      );
    });
  };

  render() {
    return (
      <>
        <h1 className="text-white">Danh sách ghế đã chọn</h1>
        <table class="table table-dark">
          <thead>
            <tr>
              <th>Số ghế</th>
              <th>Giá</th>
              <th></th>
            </tr>
          </thead>
          <tbody>{this.renderDanhSachVeDaChon()}</tbody>
        </table>

        {this.props.danhSachGheDaChon.length > 0 && (
          <button className="btn btn-success">Đặt vé</button>
        )}
      </>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    danhSachGheDaChon: state.ticketReducer.listTicket,
  };
};

export default connect(mapStateToProps)(DanhSachGheDaChon);
