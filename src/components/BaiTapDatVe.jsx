import React, { Component } from "react";
import { dataSeats } from "../data/danhSachGhe";
import HangGhe from "./HangGhe";
import "../css/BaiTapBookingTicket.css";
import DanhSachGheDaChon from "./DanhSachGheDaChon";

export default class BaiTapDatVe extends Component {
  renderHangGhe = () => {
    return dataSeats.map((rowSeat, index) => {
      return <HangGhe hang={rowSeat.hang} rowSeat={rowSeat} key={index} />;
    });
  };

  render() {
    return (
      <div
        className="bookingMovie"
        style={{
          backgroundImage: "url(./img/bgmovie.jpg)",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          height: "100vh",
        }}
      >
        <div className="overlay">
          <h1 className="text-center text-warning">Đặt vé xem phim</h1>
          <div className="row">
            <div className="col-8">
              <div className="screen"></div>
              <div className="container-fluid mt-5">{this.renderHangGhe()}</div>
            </div>
            <div className="col-4 pr-4">
              <DanhSachGheDaChon />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
