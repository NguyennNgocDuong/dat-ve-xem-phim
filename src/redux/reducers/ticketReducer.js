import { DAT_VE } from "../constants/constantDatVe"

const initialState = {
    listTicket: []
}

export default (state = initialState, { type, payload }) => {
    switch (type) {

        case DAT_VE: {
            let newListTicket = [...state.listTicket]
            newListTicket.push(payload)
            state.listTicket = newListTicket
            return { ...state }
        }

        default:
            return state
    }
}
